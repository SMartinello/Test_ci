#!/bin/bash

# VERSION=$1
# BUILD_NUMBER=$2
# CIRCLE_ARTIFACTS="${CIRCLE_ARTIFACTS:-$PWD/dist}"
#
# mkdir -p .rpmstage/usr/bin
# cp pkg/linux_amd64/consul-populate .rpmstage/usr/bin
# chmod a+x .rpmstage/usr/bin/consul-populate
#
# docker run -t -v $CIRCLE_ARTIFACTS:/output -v $PWD/.rpmstage:/.rpmstage -e "BUILD_NUMBER=$BUILD_NUMBER" -e "VERSION=$VERSION" tenzer/fpm fpm \
#   --rpm-os linux -a x86_64 \
# 	--iteration ${BUILD_NUMBER} -n consul-populate -s dir \
# 	-p /output \
# 	--rpm-user root --rpm-group users \
# 	--rpm-use-file-permissions \
# 	-t rpm -v ${VERSION} -C /.rpmstage
#
# echo "==> RPM generated:"
# ls -hl $CIRCLE_ARTIFACTS

echo "running rpm.sh"
