#!/bin/bash
#
# This script creates docker container
set -e
FULL_IMAGE_NAME=${FULL_IMAGE_NAME:-consul-populate}
VERSION=${VERSION:-local}

docker build -t ${FULL_IMAGE_NAME}:${1} .
docker build -t ${FULL_IMAGE_NAME}:latest .
