
#cd "$(dirname "$(find ./builds -type d -name Test_ci | head -1)")"

#counting number of packages with name rpm, if less the 1 then it will install all requirements
# echo "Checking if rpm pkg is installed..."
# fpm_count="$(yum list installed fpm | grep rpm | wc -l)"
# if [ $fpm_count -lt 1 ]; then
# 	# echo "Installing rpm pkg..."
# 	# yum install rpm-build rpmdevtools
# 	# echo "Creating setuptree..."
# 	# rpmdev-setuptree
# 	# echo "Installing tree..."
# 	# yum install tree -y
# 	# echo "Installations completed."
# 	yum install ruby
#   yum install rubygems
#   gem install fpm
#
# fi


echo "Moving project files into SOURCES folder..."
pwd
IFS='/' read -r -a proj_name <<< "$1"
echo "Project name: ${proj_name[-1]}"
suffix="-1"
fpm_suffix="_fpm"
#cp ${proj_name[-1]} /home/CONCUR/smartinello/rpmbuild/SOURCES/${proj_name[-1]}
cd ..
cd ..
ls
echo "Removing files..."
rm -rf Test_ci-
rm -rf ${proj_name[-1]}$suffix
rm -rf ${proj_name[-1]}$suffix$fpm_suffix
rm -rf Test_ci-1_fpm-1.0-1.x86_64.rpm
rm -rf Test_ci-1.0-1.x86_64.rpm
ls
echo "Moving files..."
cp -R ${proj_name[-1]} temp_proj_dir
mv ${proj_name[-1]} ${proj_name[-1]}$suffix
mv temp_proj_dir ${proj_name[-1]}
echo "Installing fpm..."
gem install fpm
fpm -s dir -t rpm -n ${proj_name[-1]} -v "1.0" -C ./${proj_name[-1]}$suffix
echo "Package created."
ls
