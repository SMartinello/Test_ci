#!/bin/bash
#
# This script deploys binaries and packages to artifactory
set -e

CIRCLE_ARTIFACTS="${CIRCLE_ARTIFACTS:-$PWD/dist}"
REPONAME="${REPONAME:-yum-develop-local}"

# Windows binary
curl -v --user $DOCKER_USER:$DOCKER_PASS --upload-file $CIRCLE_ARTIFACTS/pkg/windows_amd64/consul-populate.exe -X PUT "http://artifactory.concurtech.net/artifactory/util-sandbox-local/CAP/consul-populate/windows/consul-populate-$1-$2.exe"

# Linux binary
curl -v --user $DOCKER_USER:$DOCKER_PASS --upload-file $CIRCLE_ARTIFACTS/pkg/linux_amd64/consul-populate -X PUT "http://artifactory.concurtech.net/artifactory/util-sandbox-local/CAP/consul-populate/linux/consul-populate-$1-$2"

# OSX binary
curl -v --user $DOCKER_USER:$DOCKER_PASS --upload-file $CIRCLE_ARTIFACTS/pkg/darwin_amd64/consul-populate -X PUT "http://artifactory.concurtech.net/artifactory/util-sandbox-local/CAP/consul-populate/osx/consul-populate-$1-$2"


# RPM package
curl -v --user $DOCKER_USER:$DOCKER_PASS --upload-file $CIRCLE_ARTIFACTS/consul-populate-$1-$2.x86_64.rpm -X PUT "http://artifactory.concurtech.net/artifactory/$REPONAME/consul-populate-$1-$2.x86_64.rpm"
