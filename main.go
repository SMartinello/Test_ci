package main

import (
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/docopt/docopt-go"
	"github.com/hashicorp/consul/api"
	"github.com/hashicorp/hcl"
)

// GitCommit that was compiled. This will be filled in by the compiler.
var GitCommit string

const Name = "consul-populate"
const Version = "0.9"
const VersionPrerelease = "dev"

func insertKV(ipaddress string, token string, infile string, overwrite bool, cleanFolders bool) {

	config := api.DefaultConfig()
	config.Address = ipaddress
	config.Token = token

	data, err := ioutil.ReadFile(infile)
	if err != nil {
		panic(err)
	}

	client, _ := api.NewClient(config)
	kv := client.KV()

	var input map[string]map[string]string

	err = hcl.Decode(&input, string(data))
	if err != nil {
		panic(err)
	}

	if cleanFolders {
		for group, groupedVals := range input {
			for k := range groupedVals {
				path := strings.Split(strings.Join([]string{group, k}, "/"), "/")
				storage := path[0]
				if storage == "consul" {
					pathToDelete := strings.Join(path[1:3], "/")
					_, err := kv.DeleteTree(pathToDelete, &api.WriteOptions{})
					if err != nil {
						panic(err)
					}
					fmt.Println("Folder deleted: " + pathToDelete)

				} else if storage == "vault" {
					fmt.Println("Vault is not implemented yet, ignoring")
				} else {
					fmt.Println("Unknown storage backend: " + storage)
				}
			}
		}
	}

	for group, groupedVals := range input {
		for k, v := range groupedVals {
			path := strings.Split(strings.Join([]string{group, k}, "/"), "/")
			storage := path[0]
			if storage == "consul" {
				consul(kv, strings.Join(path[1:], "/"), v, overwrite)
			} else if storage == "vault" {
				fmt.Println("Vault is not implemented yet, ignoring")
			} else {
				fmt.Println("Unknown storage backend: " + storage)
			}
		}
	}
}

func consul(kvClient *api.KV, path string, value string, overwrite bool) {
	if overwrite {
		p := &api.KVPair{Key: path, Value: []byte(value)}
		_, err := kvClient.Put(p, nil)
		if err != nil {
			panic(err)
		}
		fmt.Println("Written: " + path + ": " + value)
	} else {
		kv, _, err := kvClient.Get(path, &api.QueryOptions{})
		if err != nil {
			panic(err)
		}
		if kv == nil {
			p := &api.KVPair{Key: path, Value: []byte(value)}
			_, err := kvClient.Put(p, nil)
			if err != nil {
				panic(err)
			}
			fmt.Println("Written: " + path + ": " + value)
		} else {
			fmt.Println("Already exists, skipping: " + path)
		}
	}
}

func main() {

	usage := `Consul Key Value populate tool. Requires FILE in HCL language format:

"consul/private/concur-logging" {
  "some-private-value" = "private!"
  "private-folder/another-private-value" = "private!"
}

"consul/public/concur-logging" {
  "rabbitmq/logging/exchange" = "logging-json"
  "rabbitmq/logging/vhost" = "logging"
}

"consul/public/concur-logging/rabbitmq" {
  "logging/user" = "logging"
  "logging/routingKey" = "error"
}

Usage:
  consul-populate [-o] [-c] [-i IP:PORT] [-t TOKEN] FILE
  consul-populate -h | --help
  consul-populate --version

Options:
  -h --help                          Show this screen.
  --version                          Show version.
  -i, --address=IP:PORT              The HTTP endpoint of Consul [default: 127.0.0.1:8500].
  -t, --token=TOKEN                  An ACL Token with proper permissions in Consul [default: ].
  -o, --overwrite                    Overwrite existing key value pairs.
  -c, --clean-folders                Remove values which are not defined in source FILE`

	arguments, _ := docopt.Parse(usage, nil, true, "consul-populate "+Version, false)
	fmt.Println(arguments)
	insertKV(arguments["--address"].(string), arguments["--token"].(string), arguments["FILE"].(string), arguments["--overwrite"].(bool), arguments["--clean-folders"].(bool))
}
