FROM golang:1.4-alpine
# This container is meant mainly for development. It is possible to use it in production, but for that purpose it should be cleaned up.
ENTRYPOINT ["/bin/consul-populate"]

ADD . /go/src/github.concur.com/CAP/consul-populate
WORKDIR /go/src/github.concur.com/CAP/consul-populate
RUN apk add --no-cache git mercurial; go get
RUN go build -o /bin/consul-populate
