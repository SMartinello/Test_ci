TEST?=./...
NAME = $(shell awk -F\" '/^const Name/ { print $$2; exit }' main.go)
VERSION = $(shell awk -F\" '/^const Version/ { print $$2; exit }' main.go)
CIRCLE_BUILD_NUM?=local
REPONAME?=yum-sandbox-local

default: test

# bin generates the releasable binaries
bin: generate
	@sh -c "'$(CURDIR)/scripts/build.sh'"

# dev creates binares for testing locally. There are put into ./bin and $GOPAHT
dev: generate
	@CT_DEV=1 sh -c "'$(CURDIR)/scripts/build.sh'"

# dist creates the binaries, packages and docker image for distibution
rpm: bin
	@sh -c "'$(CURDIR)/scripts/rpm.sh' $(VERSION) $(CIRCLE_BUILD_NUM)"

docker: generate
	@sh -c "'$(CURDIR)/scripts/docker.sh' $(VERSION).$(CIRCLE_BUILD_NUM)"

# Deploy to artifactory (run after dist)
artifactory:
	@sh -c "'$(CURDIR)/scripts/artifactory_deploy.sh' $(VERSION) $(CIRCLE_BUILD_NUM)"

# test runs the test suite and vets the code
test: generate
	go test $(TEST) $(TESTARGS) -timeout=30s -parallel=4

# testrace runs the race checker
testrace: generate
	go test -race $(TEST) $(TESTARGS)

# updatedeps installs all the dependencies
updatedeps:
	go get -u github.com/mitchellh/gox
	go get -f -t -u ./...

# generate runs `go generate` to build the dynamically generated
# source files.
generate:
	@find . -type f -name '.DS_Store' -delete
	go generate ./...

.PHONY: default bin dev rpm test testrace updatedeps docker artifactory generate
